#Required
#crc32.exe

#This Script will rename all folders and files to GXS's standard naming convention.
#Script will use a ultility to generate CRCs
#if a CRC is detected at the end of the file, will skip CRC generation for that file.
#Therefore make sure to generate a Correct CRC for new files before running this script or
#remove the existing CRC to generate a new one.

#This script checks the Directory from which it is ran.
#It will only affect folders with the key-word "project-gxs" and .mkv files

#Current Naming Convention:
#     Directory: [project-gxs]_Anime_Series_Name_(10bit_BD_1080p)
#     Files:     [project-gxs]_Anime_Series_Name_-_XX_(10bit_BD_1080p)_[CRC].mkv


function Main {
    $path = $PSScriptRoot
    $crcUtilPath = ($PSScriptRoot + "\crc32.exe")
    $currentPath = Get-Location

    do {
        ShowMenu

        $input = Read-Host "Please select an option"

        if ($input.ToLower() -eq 'v') {
            ViewKillZone
        }
        elseif ($input.ToLower() -eq 'p') {
            StartRename $input.ToLower()
        }
        elseif ($input.ToLower() -eq 's') {
            StartRename $input.ToLower()
        }
        elseif ($input.ToLower() -eq 'q') {
            "--- Exiting ---"
            exit
        }
        else {
            clear
            "--- Invalid Selection ---"
        }
    } while ($input.ToLower() -ne 's')
    exit
}

#Menu Prompt for Renaming Selections
function ShowMenu {
    "RecursiveRename Menu"
    ""
    "(V)iew KillZone"
    "(P)review Changes"
    ""
    "(S)tart Rename Process"
    "(Q)uit`n"
}

function ViewKillZone {
    $currentLevel = 0
    clear
    " ----- KillZone ----- "
    ""
    $lvl1Dir = ls -Directory -Filter *project-gxs*

    foreach ($directory in $lvl1Dir) {
        if ($currentLevel -eq 0) {
            Get-Location | Split-Path -Leaf
            $currentLevel++
        }
        "+---$directory"
        foreach ($file in ls -LiteralPath $directory *.mkv) {
            "|   |   $file"
        }
        foreach ($subdirectory in ls $directory -Directory) {
            "|   +---$subdirectory"
            foreach ($file in ls $directory\$subdirectory -File -Filter *.mkv) {
                "|   |   |   $file"
            }
        }
    }
    ""
}


function StartRename($selection) {
    $currentLevel = 0
    clear
    if ($selection -eq 'p') {
        " ----- Preview ----- "
    }
    elseif ($selection -eq 's') {
        " ----- Renaming Files ----- "
    }
    "Use Ctrl+C to force stop script"
    ""
    $lvl1Dir = ls -Directory -Filter *project-gxs*
    $genCrc = $true

    if (!(Test-Path -LiteralPath $crcUtilPath)) {
        "Unable to locate crc32.exe in script path. Disabled CRC Generation"
        $genCrc = $False
    }

    foreach ($directory in $lvl1Dir) {
        if ($currentLevel -eq 0) {
            Get-Location | Split-Path -Leaf
            $currentLevel++
        }
        "+---$directory"
        foreach ($file in ls -LiteralPath $directory *.mkv) {
            $animeTitle = $file.Name
            $subgroup = ""
            $mediaInfo = ""
            $crc = $file.Name[-14..-5] -join ""

            ParseCrc([ref]$crc)([ref]$animeTitle) $genCrc
            ParseMediaInfo([ref]$animeTitle)([ref]$subgroup)([ref]$mediaInfo)
            BuildNewName([ref]$animeTitle) $subgroup $mediaInfo $crc

            "|   |   $file"
            write-host "|   |   $animeTitle" -ForegroundColor Magenta

            if ($selection -eq 's') {
                Rename-Item -LiteralPath $file.FullName $animeTitle
            }
        }
        foreach ($subdirectory in ls $directory -Directory) {
            "|   +---$subdirectory"
            foreach ($file in ls $directory\$subdirectory -File -Filter *.mkv) {
                $animeTitle = $file.Name
                $mediaInfo = ""
                $crc = $file.Name[-14..-5] -join ""

                ParseCrc([ref]$crc)([ref]$animeTitle) $genCrc
                ParseMediaInfo([ref]$animeTitle)([ref]$subgroup)([ref]$mediaInfo)
                BuildNewName([ref]$animeTitle) $subgroup $mediaInfo $crc

                "|   |   |   $file"
                write-host "|   |   |   $animeTitle" -ForegroundColor Magenta

                if ($selection -eq 's') {
                    Rename-Item -LiteralPath $file.FullName $animeTitle
                }
            }
        }
    }
    ""
}

function ParseCrc ([ref]$crc, [ref]$animeTitle, $genCrc) {
    $hex = "1234567890abcdefABCDEF"

    #Validates CRC format, if valid keeps $crc
    if ($crc.Value[0] -ne '[' -or $crc.Value[9] -ne ']') {
        #"Invalid CRC Format"
        $crc.Value = $null
    }
    else {
        foreach ($value in $crc.Value[1..8]) {
            if (!$hex.contains($value)) {
                #"Invalid CRC Value"
                $crc.Value = $null
            }
        }
    }
    #If invalid CRC format, generate CRC with brackets if utility exist
    if ($crc.Value -eq $null) {
        if ($genCrc) {
            Set-Location -LiteralPath $PSScriptRoot
            $crc.Value = (& .\crc32.exe $file.FullName -noformat)[0]
            $crc.Value = "[" + $crc.Value + "]"
            cd $currentPath
        }
    }
    else {
        $animeTitle.Value = $animeTitle.Value -Replace [regex]::Escape($crc.Value),""
    }
}

function ParseMediaInfo([ref]$animeTitle, [ref]$subgroup, [ref]$mediaInfo) {
    $info1 = [regex]::match($animeTitle.Value,'\[[^\]]+\]').Groups[0].Value
    if ($info1 -eq "") {
        $info1 = [regex]::match($animeTitle.Value,'\([^\)]+\)').Groups[0].Value
    }
    
    $animeTitle.Value = $animeTitle.Value -Replace [regex]::Escape($info1),""

    $info2 = [regex]::match($animeTitle.Value,'\[[^\]]+\]').Groups[0].Value
    if ($info2 -eq "") {
        $info2 = [regex]::match($animeTitle.Value,'\([^\)]+\)').Groups[0].Value
    }

    $animeTitle.Value = $animeTitle.Value -Replace [regex]::Escape($info2),""
    $animeTitle.Value = $animeTitle.Value.trim(" _") -replace " ","_"

    while ($animeTitle.Value[-5] -eq "_") {
        $animeTitle.Value = $animeTitle.Value.substring(0,$animeTitle.Value.length-5) + $animeTitle.Value.Substring($animeTitle.Value.length-4)
    }

    #find resolution value by looking for 3 consecutive digits
    $consecutiveDigits = 0
    foreach ($val in [char[]]$info1) {
        if ($val -match "[0-9]") {
            $consecutiveDigits++
            if ($consecutiveDigits -eq 3) {
                $subgroup.Value = $info2
                $mediaInfo.Value = $info1
                return
            }
        }
        else {
            $consecutiveDigits = 0
        }
    }
    $subgroup.Value = $info1
    $mediaInfo.Value = $info2
}

function BuildNewName([ref]$animeTitle, $subgroup, $mediaInfo, $crc) {
    $mediaInfo = $mediaInfo -replace " ","_"
    $mediaInfo = "[" + $mediaInfo.substring(1,$mediaInfo.length-2) + "]"
    $animeTitle.Value = "[project-gxs]_" + $animeTitle.Value.Substring(0,$animeTitle.Value.length-4) + "_" + "$mediaInfo" + "_" + "$crc.mkv"
}

Main
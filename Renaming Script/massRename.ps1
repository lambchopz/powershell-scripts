#This is a simple renaming script meant to rename all *mkv files detected in the defined directory
#It will rename the files in order and starting with '00'
#Slight modifications can be made

function Main {
    $path = $PSScriptRoot
    $crcUtilPath = ($PSScriptRoot + "\crc32.exe")

    $prefix = '(project-gxs)_FileName_-_'
    $suffix = '_(10bit_720p)'
    $keepCrc = $true
    $genCrc = $false

    do { 
        ShowMenu

        $input = Read-Host "Please select an option"

        if ($input.ToLower() -eq 'd') {
            SetDirectory([ref]$path)
        }
        elseif ($input.ToLower() -eq 'p') {
            SetPrefix([ref]$prefix)
        }
        elseif ($input.ToLower() -eq 's') {
            SetSuffix([ref]$suffix)
        }
        elseif ($input.ToLower() -eq 'r') {
            FlipCRCRetention([ref]$keepCrc)([ref]$genCrc)
        }
        elseif ($input.ToLower() -eq 'g') {
            FlipCRCGeneration([ref]$genCrc)([ref]$keepCrc)
        }
        elseif ($input.ToLower() -eq '0') {
            PreviewRename $prefix $suffix $path $keepCrc $genCrc
        }
        elseif($input.ToLower() -eq '1') {
            RenameFiles $prefix $suffix $path $keepCrc $genCrc
        }
        elseif ($input.ToLower() -eq 'q') {
            "--- Exiting ---"
            exit
        }
        else {
            clear
            "--- Invalid Selection ---"
        }
    } while ($input -ne 1)
    exit
}

#Menu Prompt for Renaming Selections
function ShowMenu {
    "MassRename Menu"
    ""
    "Set (D)irectory Path: `"$path`""
    "Set (P)refix: `"$prefix`""
    "Set (S)uffix: `"$suffix`""
    "Invert (R)etain Current CRC Value: $keepCrc"
    "Invert (G)enerate and append CRC32 Value: $genCrc"
    "(0) Show Renaming Preview"
    "(1) Start Renaming Process"
    "(Q)uit`n"
}

#Set Directory Selection
#'D' to set Directory path to script location
#'C' to cancel and exit to Menu Prompt
#Otherwise checks the validity of entered path
function SetDirectory([ref]$path) {
    clear
    "--- Set Directory Path ---`n"
    $CurPath = $path.value
    "Current Path: `"$CurPath`""
    "Enter 'D' to set path to current script location"
    "Enter 'C' to cancel and return to menu.`n"

    $CurPath = Read-Host -Prompt "New Path"

    if ($CurPath.length -eq 1) {
        if ($CurPath.ToLower() -eq 'c') {
            clear
            "--- Exited: Set Directory Path ---"
        }
        elseif ($CurPath.ToLower() -eq 'd') {
            clear
            "--- Directory Path Set to Script Location ---"
            $path.value = $PSScriptRoot
        }
    } 
    elseif (Test-Path -Literalpath $CurPath) {
        clear
        "--- New Directory Path Set ---"
        $path.value = $CurPath
    }
    else {
        clear
        "--- Invalid Directory Path Entered ---"
    }
}

#Set Prefix Selection
#'C' to cancel and exit to Menu Prompt
#No error check on invalid characters (Though when attempting to rename files using invalid characters, PS will probably yell at you)
function SetPrefix([ref]$prefix) {
    clear
    "--- Set Prefix ---`n"
    $CurPrefix = $prefix.value
    "I'm too lazy to error check these invalid characters so don't use them!:"
    "\ / : * ? `" < > |`n"

    "Current Prefix: `"$CurPrefix`""
    "Enter 'C' to cancel and return to menu.`n"

    $CurPrefix = Read-Host -Prompt "New Prefix"

    if ($CurPrefix.length -eq 1 -and $CurPrefix.ToLower() -eq 'c') {
        clear
        "--- Exited: Set Prefix ---"
    }
    else {
        clear
        $prefix.value = $CurPrefix
        "--- New Prefix Set ---"
    }
}


#Set Suffix Selection
#'C' to cancel and exit to Menu Prompt
#No error check on invalid characters (Though when attempting to rename files using invalid characters, PS will probably yell at you)
function SetSuffix([ref]$suffix) {
    clear
    "--- Set Prefix ---`n"
    $CurSuffix = $suffix.value

    "I'm too lazy to error check these invalid characters so don't use them!:"
    "\ / : * ? `" < > |`n"

    "Current Suffix: `"$CurSuffix`""
    "Enter 'C' to cancel and return to menu.`n"

    $CurSuffix = Read-Host -Prompt "New Suffix"

    if ($CurSuffix.length -eq 1 -and $CurSuffix.ToLower() -eq 'c') {
        clear
        "--- Exited: Set Suffix ---"
    }
    else {
        clear
        $suffix.value = $CurSuffix
        "--- New Suffix Set ---"
    }
}


#Enable/Disables CRC Retention
#'C' to cancel and exit to Menu Prompt
#This function simply Flips the CRC Retention value from True to False or False to True
function FlipCRCRetention([ref]$keepCrc, [ref]$genCrc) {
    clear
    if ($keepCrc.value) {
        $keepCrc.value = $False
        "--- Disabled: Retain Current CRC Value ---"
    }
    else {
        $keepCrc.value = $True
        "--- Enabled: Retain Current CRC Value ---"
        $genCrc.value = $False
        "--- Disabled: Generate and Append CRC ---"
    }
}

function FlipCRCGeneration([ref]$genCrc, [ref]$keepCrc) {
    clear
    if ($genCrc.value) {
        $genCrc.value = $False
        "--- Disabled: Generate and Append CRC ---"
    }
    else {
        $genCrc.value = $True
        "--- Enabled: Generate and Append CRC ---"
        $keepCrc.value = $False
        "--- Disabled: Retain Current CRC Value --- "
    }
}


#This function previews how each file is going to be rename once the process is started
#Checks if CRC Retention or Generation is enabled.
function PreviewRename($prefix, $suffix, $path, $retainCrc, $genCrc) {
   clear
    "--- Preview Rename ---`n"

    "path: `"$path`""
    "prefix: `"$prefix`""
    "suffix:`" $suffix`""
    "Retain CRC: $retainCrc`n"

    $EpisodeNum = 1
    $files = ls -LiteralPath $path *.mkv
    $crc = ""

    foreach ($file in $files) {
        if ($retainCrc) {
            
            $crc = $file.Name[-14..-5] -join ''
            $hex = "1234567890abcdefABCDEF"
            
            if ($crc[0] -ne '[' -or $crc[9] -ne ']') {
                "Invalid CRC Format, Removed CRC Retention"
                $crc = ""
            }
            else {
                foreach ($value in $crc[1..8]) {
                    
                    if (!$hex.contains($value)) {
                        "Invalid CRC Value, Removed CRC Retention"
                        $crc = ""
                    }
                }
                if ($crc -ne "") {
                    $crc = '_' + $crc
                }
            }
        }
        if ($genCrc) {
            if (!(Test-Path -LiteralPath $crcUtilPath)) {
                "Unable to locate crc32.exe in script path. Disabled CRC Generation"
                $genCrc = $False
            }
            else {
                Set-Location -LiteralPath $PSScriptRoot
                $crc = (& .\crc32.exe ($path + '\' + $file) -noformat)[0]
                $crc = "_[$crc]"
            }
        }

	    "Renaming file:" 
	    '    "' + $file.Name + '" to'

	    if ($EpisodeNum -lt 10) {
		    $NewFileName = $prefix + '0' + $EpisodeNum + $suffix + $crc + '.mkv'
		    '    "' + $NewFileName
	    }
	    Else {
		    $NewFileName = $prefix + $EpisodeNum + $suffix + $crc + '.mkv'
		    '    "' + $NewFileName
	    }
        ""
	    $EpisodeNum++
    }

    if ($EpisodeNum -eq 1) {
        "Warning: No mkv video files were detected in `"$path`""
    }
    ""
    pause
    clear
}

#This function starts the renaming process and will rename all files listed by Preview
#There is a single check before the renaming starts
function RenameFiles($prefix, $suffix, $path, $retainCrc, $genCrc) {
    clear
    "--- Renaming Files ---`n"

    "path: `"$path`""
    "prefix: `"$prefix`""
    "suffix:`" $suffix`""
    "Retain CRC: $retainCrc`n"

    $EpisodeNum = 1
    $files = ls -LiteralPath $path *.mkv
    $crc = ""

    foreach ($file in $files) {
        if ($retainCrc) {
            
            $crc = $file.Name[-14..-5] -join ''
            $hex = "1234567890abcdefABCDEF"
            
            if ($crc[0] -ne '[' -or $crc[9] -ne ']') {
                "Invalid CRC Format, Removed CRC Retention"
                $crc = ""
            }
            else {
                foreach ($value in $crc[1..8]) {
                    
                    if (!$hex.contains($value)) {
                        "Invalid CRC Value, Removed CRC Retention"
                        $crc = ""
                    }
                }
                if ($crc -ne "") {
                    $crc = '_' + $crc
                }
            }
        }
        if ($genCrc) {
            if (!(Test-Path -LiteralPath $crcUtilPath)) {
                "Unable to locate crc32.exe in script path. Disabled CRC Generation"
                $genCrc = $False
            }
            else {
                Set-Location -LiteralPath $PSScriptRoot
                $crc = (& .\crc32.exe ($path + '\' + $file) -noformat)[0]
                $crc = "_[$crc]"
            }
        }

	    "Renaming file:" 
	    '    "' + $file.Name + '" to'

	    if ($EpisodeNum -lt 10) {
		    $NewFileName = $prefix + '0' + $EpisodeNum + $suffix + $crc + '.mkv'
            Rename-Item -LiteralPath ($path + '\' + $file) $NewFileName
		    '    "' + $NewFileName
	    }
	    Else {
		    $NewFileName = $prefix + $EpisodeNum + $suffix + $crc + '.mkv'
            Rename-Item -LiteralPath ($path + '\' + $file) $NewFileName
		    '    "' + $NewFileName
	    }
        ""
	    $EpisodeNum++
    }

    if ($EpisodeNum -eq 1) {
        "Warning: No mkv video files were detected in `"$path`""
    }
}

Main